Role Name
=========

Sets up a sway wm workstation for development.

Requirements
------------

Must be manageable by ansible.

Role Variables
--------------

The user account to configure the workstation for:

    username: dbuckley

URL to the wallpaper to download (optional):

    wallpaper: http://download.com/wallpaper.jpg

List of dnf packages to install (optional):

    packages:
      - htop

List of copr user/repositories to add (optional):

    copr:
      - user: livegrenier
        repo: i3-desktop

List of flatpaks to install (optional):

    flatpaks:
      - com.valvesoftware.Steam
      - org.telegram.destop

List of python packages to install (optional):

    pip_packages:
      - molecule


Dependencies
------------

N/A

Example Playbook
----------------

    - hosts: workstations
      roles:
         - { role: ansible-workstation-role, username: dbuckley }

License
-------

GPLv2

Author Information
------------------

Derek G Buckley
dgbuckley@protonmail.com
